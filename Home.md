#**Survive Wiki**#
#**1. プロジェクト概要**#
----
　Surviveに配属されたら以下の資料に目を通し、プロジェクトの概要把握を行って下さい。    
　また、目を通すだけでなく、必ず上長から説明を受け、共通した認識を持ってプロジェクトの開発に携わるようにしてください。

  **+ 企画概要書[[表示する](https://bytebucket.org/tack_miyamoto/survive-wiki/raw/e7b2561a04dba8e99e7841d10cd8d29e77c924f2/Survive_outline_1017.pdf)]**

> Surviveの企画概要書です。  
> ゲームのコンセプト、プロジェクトが目指すゴール、実現したい体験、世界観、マネタイズ等々、Surviveの概要をまとめた資料です。    
  
  **+ UI、UX、Visualガイドライン[[表示する](https://bytebucket.org/tack_miyamoto/survive-wiki/raw/e6717799aa26c9f4a8c42c657c3d1b6a7f47e581/Survive%20Guideline%20vol.01.pdf)]**

> UI、UX、Visualなどの、開発に関するSruviveのガイドラインです。


**+ 世界観[[表示する](https://bytebucket.org/tack_miyamoto/survive-wiki/raw/e6717799aa26c9f4a8c42c657c3d1b6a7f47e581/Survive%20Visual%20Concept.pdf)]**

> Surviveの世界観をまとめた資料です。

#**2. アクセス権限各種**#
----
　Surviveの開発で必要になるネットワークフォルダ等のアクセス権限は以下の通りです。    
　プロジェクトにアサインされた時点で設定されているものもあるかも知れませんが、漏れがないか確認して下さい。  
  設定されていないものは上長に相談の上、申請手続きを行って下さい。

  **+ ネットワークフォルダ(\\gfs\Shares\Survive)**

> Surviveで使用する共有のネットワークフォルダです。  
  
  **+ Jira[[表示する](https://jira.clayapp.jp/browse/SURVIVE)]**

> タスク管理に使用しているBTSです。


  **+ BitBucket[[表示する](https://bitbucket.org/)]**

> SurviveではGitで開発のバージョン管理を行っており、そのホスティングサービスとしてBitBucketを利用しています。  
> Git及びBitBucketの利用方法については後述しています。

#**3. Gitでのバージョン管理について**#
----
　前述のとおりSurviveではGitによるバージョン管理を行っています。    
　運用のルール等を以下の資料にまとめたので、確認して下さい。

  **+ BitBucketについて（作成中）[[表示する](https://bytebucket.org/tack_miyamoto/survive-wiki/raw/8f0fb08f94e653beebc5aae9da09129ce59ceee1/BitBucket%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6.xlsx)]**



